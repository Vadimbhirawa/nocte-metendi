extends KinematicBody2D

var movespeed = 500
var bulletSpeed = 2000
var bullet = preload("res://Scenes/Bullet.tscn")
var animation = "normal"
var dead = false

func _physics_process(_delta):
	var move = Vector2()
	if dead == false:
		if Input.is_action_pressed("ui_up"):
			move += Vector2(0, -1)
			get_parent().get_tree().paused = false
		if Input.is_action_pressed("ui_down"):
			move += Vector2(0, 1)
			get_parent().get_tree().paused = false
		if Input.is_action_pressed("ui_left"):
			move += Vector2(-1, 0)
			get_parent().get_tree().paused = false
		if Input.is_action_pressed("ui_right"):
			move += Vector2(1, 0)
			get_parent().get_tree().paused = false
		
		move = move.normalized()
		move_and_slide(move * movespeed)
		look_at(get_global_mouse_position())
	
		if Input.is_action_just_pressed("ui_click"):
			fire()
	
func fire():
	var bullet_instance = bullet.instance()
	bullet_instance.position = get_global_position()
	bullet_instance.rotation_degrees = rotation_degrees
	bullet_instance.apply_impulse(Vector2(),Vector2(bulletSpeed, 0).rotated(rotation))
	get_tree().get_root().call_deferred("add_child", bullet_instance)
	$GunAudio.play()
	
func kill():
	animation = "death"
	dead = true
	
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)
		
	get_tree().change_scene(str("res://Scenes/GameOver.tscn"))

func _on_Area2D_body_entered(body):
	if "Enemy" in body.name:
		kill()
