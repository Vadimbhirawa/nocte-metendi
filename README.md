# Individual GameJam - Nocte Metendi



## The Night of Reaping is upon us!

Nocte Metendi is a survival horror game where the player themselves can't even see straight! Play as Burnham, a former soldier who's thrown back into action because of mysterious events happening around his home town of Black Brick. Navigate through dark corners, make sure no "reapers" follow you, and shoot them dead if you see one! This was made as an attempt to fuse the classic Resident Evil atmosphere of not knowing what awaits you next, and classic top-down shooters.

## Welcome to Black Brick...

Black Brick has always been a quiet little town. But at least it used to be full of neighbors arguing every Sunday Morning, kids in the park playing pranks, and the crazy cat lady who's always smoking 5 packs a day. However, things are different now. Everyone's keeping themselves indoors, no one's outside playing and chatting anymore. 

Burnham came home to Black Brick after 7 long years of fighting, hoping to enjoy the mundane atmosphere found at his home town. What awaits him is anything but mundane. Missing person cases, black goo spreading around the town, and reports of mysterious creatures roaming around the town at night to "reap" sinners.

One night, Burnham awoke to chaos in his backyard. Odd creatures called the "reapers" started roaming around the town openly and caused chaos by taking the heads of civilians and turning those civilians into one of them. Equipped with his trusty pistol, flashlight, and fighting instincts, Burnham must roam the dark alleys of Black Brick in search of a "safe area", said to purge any "reapers" that step inside.

### Features

- Resident Evil-inspired horrors of not knowing whether or not you're safe until you reach a safe area.
- Limited Lighting: The "reapers" have turned off the lights around Black Brick. Burnham has to navigate around the town's alley in pitch black darkness only armed with the light from his flashlight.
- Instinct Mode: Burnham's years of experience out in the battlefield has heightened his senses to almost a superhuman degree. As long as Burnham stays still, time itself would appear as though it has stopped.

### Controls
- W: Move up.
- A: Move left.
- S: Move down.
- D: Move right.
- Left Mouse: Shoot.

### Installation
- Extract Nocte Metendi.zip
- Launch Nocte Metendi.exe