extends KinematicBody2D

var move = Vector2()

func _ready():
	pass # Replace with function body.

func _physics_process(_delta):
	var Player = get_parent().get_node("Player")
	
	position += (Player.position - position)/50
	look_at(Player.position)
	
	move_and_collide(move)

func _on_Area2D_body_entered(body):
	if "Bullet" in body.name:
		queue_free()


func _on_Area2D_area_entered(area):
	if "AreaTrigger" in area.name:
		queue_free()
